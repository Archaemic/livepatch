#include "patch.h"
#include "pspsdk.h"

PSP_MODULE_INFO("livepatch", PSP_MODULE_KERNEL, 1, 0);

typedef int (*modloadHandler)(SceModule2 *);

SceUID debugFile = 2;
char debugHex[8] = "00000000";

modloadHandler sctrlHENSetStartModuleHandler(modloadHandler handler);
static modloadHandler old = 0;

static SceUID thid;
static SceUID pipeid;
static volatile int exiting = 0;

static struct {
	char root[15];
	char module[64];
} __attribute__((__packed__)) patchPath;

static SceUID patchedModules[16] = {
	0x80000000,
	0x80000000,
	0x80000000,
	0x80000000,
	0x80000000,
	0x80000000,
	0x80000000,
	0x80000000,
	0x80000000,
	0x80000000,
	0x80000000,
	0x80000000,
	0x80000000,
	0x80000000,
	0x80000000,
	0x80000000
};

static int modload(SceModule2* m) {
	if (m != 0 && exiting == 0) {
		if (m->text_addr & 0x80000000) {
			DPRINT("Skipping kernel module ");
			DPRINT(m->modname);
			DPRINT("\n");
		} else if (m->modname[0] == 's' && m->modname[1] == 'c' && m->modname[2] == 'e') {
			DPRINT("Skipping Sony module ");
			DPRINT(m->modname);
			DPRINT("\n");
		} else {
			sceKernelSendMsgPipe(pipeid, m, sizeof(SceModule2), 0, 0, 0);
		}
	}
	return old != 0 ? old(m) : 0;
}

int patchThread(SceSize args, void *argp) {
	SceModule2 mod;
	strcpy(patchPath.root, "ms0:/livepatch/");
	old = sctrlHENSetStartModuleHandler(modload);
	pipeid = sceKernelCreateMsgPipe("pipeLoad", 1, 0, 0, 0);
	DPRINT("LivePatch initialized\n");
	while (1) {
		int res = sceKernelReceiveMsgPipe(pipeid, &mod, sizeof(mod), 0, 0, 0);
		if (res < 0 || exiting != 0) {
			DPRINT("Stopping patch thread...\n");
			break;
		}
		int terminator;
		for (terminator = 0; terminator < sizeof(patchPath.module) - 1; ++terminator) {
			patchPath.module[terminator] = mod.modname[terminator];
			if (patchPath.module[terminator] == 0) {
				break;
			}
		}
		DPRINT("Found module ");
		DPRINT(mod.modname);
		DPRINT("\n");
		patchPath.module[terminator] = '/';
		++terminator;
		patchPath.module[terminator] = '\0';

		int patchId = 0;
		for (; patchId < sizeof(patchedModules) / sizeof(SceUID); ++patchId) {
			if (patchedModules[patchId] < 0) {
				break;
			}
			if (patchedModules[patchId] == mod.modid) {
				break;
			}
		}

		if (patchId >= sizeof(patchedModules) / sizeof(SceUID)) {
			DPRINT("No free patches left!\n");
			break;
		}

		if (patchedModules[patchId] == mod.modid) {
			DPRINT("Module already patched.\n");
			continue;
		}
		SceIoDirent dirent;
		SceUID dfd = sceIoDopen((char*) &patchPath);
		if (dfd >= 0) {
			memset(&dirent, 0, sizeof(dirent));
			while (sceIoDread(dfd, &dirent) > 0) {
				if (dirent.d_name[0] == '.') {
					// Ignore dotfiles
					continue;
				}
				int i;
				for (i = 0; i + terminator < sizeof(patchPath.module) - 1; ++i) {
					patchPath.module[i + terminator] = dirent.d_name[i];
					if (dirent.d_name[i] == 0) {
						break;
					}
				}
				DPRINT("Found patch ");
				DPRINT(patchPath.module);
				DPRINT("\n");
				int fd = sceIoOpen((char *) &patchPath, PSP_O_RDONLY, 0777);
				if (fd >= 0) {
					applyPatch(fd, &mod);
					sceIoClose(fd);
					patchedModules[patchId] = mod.modid;
				} else {
					DPRINT("Could not open patch file: ");
					DHEX(fd);
					DPRINT("\n");
				}
				memset(&dirent, 0, sizeof(dirent));
			}

			sceIoDclose(dfd);
			if (patchedModules[patchId] == mod.modid) {
				DPRINT("Patchset applied.\n");
			} else {
				DPRINT("No patches found in patch folder.\n");
			}
		} else {
			DPRINT("No associated patch folder found: ");
			DHEX(dfd);
			DPRINT("\n");
		}
	}
	DPRINT("Stopped!\n");
	exiting = 1;
	sceKernelCancelMsgPipe(pipeid, 0, 0);
	sceKernelDeleteMsgPipe(pipeid);
	sctrlHENSetStartModuleHandler(old);
	sceKernelExitThread(0);
	return 0;
}

int module_start(SceSize args, void *argp) {
	thid = sceKernelCreateThread("patchthread", patchThread, 0x10, 0x1000, 0, 0);
	if (thid < 0) {
		DPRINT("Error starting patching thread: ");
		DHEX(thid);
		DPRINT("\n");
		return thid;
	}
	#ifdef DEBUG
	debugFile = sceIoOpen("ms0:/livepatch/log.txt", PSP_O_WRONLY | PSP_O_CREAT | PSP_O_TRUNC, 0777);
	if (debugFile < 0) {
		debugFile = 2;
	}
	#endif
	return sceKernelStartThread(thid, 0, 0);
}

int module_stop(SceSize args, void *argp) {
	DPRINT("Attempting to exit\n");
	exiting = 1;
	sceKernelCancelMsgPipe(pipeid, 0, 0);
	sceKernelWaitThreadEnd(thid, 0);
	sceKernelDeleteThread(thid);
	#ifdef DEBUG
	if (debugFile != 2) {
		sceIoClose(debugFile);
		debugFile = 2;
	}
	#endif
	return 0;
}
