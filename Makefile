TARGET = livepatch
OBJS = main.o patch.o imports.o exports.o

USE_KERNEL_LIBC = 1
USE_KERNEL_LIBS = 1

CFLAGS = -Wall -fno-builtin-printf -O0 -G0 -fno-pic
ASFLAGS = $(CFLAGS)
LDFLAGS=-nodefaultlibs -lpspkernel $(CFLAGS)

PSPSDK=$(shell psp-config --pspsdk-path)
include $(PSPSDK)/lib/build_prx.mak

