#include "common.h"

#define MAGIC 0x6576694C

enum MemPatchFlags {
	XOR_DATA = 1,
	NULL_DATA = 2,
	TEXT_REL = 4
};

struct PatchHeader {
	unsigned int magic;
	unsigned int formatVersion;
	unsigned int nPatches;
	unsigned int reserved;
} __attribute__((__packed__));

struct MemPatch {
	unsigned int fileOffset;
	unsigned int memOffset;
	unsigned int size;
	unsigned int flags;
} __attribute__((__packed__));

void applyPatch(SceUID fd, SceModule2* mod);
