#include "patch.h"
#include <pspsdk.h>

void patchMemory(SceUID fd, struct MemPatch* patchinfo) {
	if ((patchinfo->memOffset & 0xF8000000) != 0x08000000 || ((patchinfo->memOffset + patchinfo->size) & 0xF8000000) != 0x08000000) {
		DPRINT("Invalid access in patch");
		return;
	}

	if (patchinfo->flags & NULL_DATA) {
		int intr = pspSdkDisableInterrupts();
		memset((unsigned int*) patchinfo->memOffset, 0, patchinfo->size);
		pspSdkEnableInterrupts(intr);
	} else {
		if (sceIoLseek32(fd, patchinfo->fileOffset, PSP_SEEK_SET) != patchinfo->fileOffset) {
			DPRINT("Premature end of file.\n");
			return;
		}

		unsigned int readBuffer[128];
		unsigned int* memBuffer = (unsigned int*) patchinfo->memOffset;
		int remaining;
		int toRead = sizeof(readBuffer);
		for (remaining = patchinfo->size; remaining > 0; remaining -= toRead) {
			if (toRead > remaining) {
				toRead = remaining;
			}

			int unaligned = (unsigned int) memBuffer & (sizeof(unsigned int) - 1);
			if (unaligned != 0) {
				toRead = unaligned;
			}

			if (sceIoRead(fd, readBuffer, toRead) < toRead) {
				DPRINT("Premature end of file while applying patch!");
				return;
			}

			int i;
			int intr = pspSdkDisableInterrupts();
			if (patchinfo->flags & XOR_DATA) {
				for (i = 0; (i + sizeof(unsigned int) - 1) < toRead; i += sizeof(unsigned int)) {
					memBuffer[i] ^= readBuffer[i];
				}
				unaligned = toRead & (sizeof(unsigned int) - 1);
				if (unaligned != 0) {
					char* charMemBuffer = (char*) &memBuffer[toRead / sizeof(unsigned int)];
					char* charReadBuffer = (char*) &readBuffer[toRead / sizeof(unsigned int)];
					for (i = 0; i < unaligned; ++i) {
						charMemBuffer[i] ^= charReadBuffer[i];
					}
				}
			} else {
				memcpy(memBuffer, readBuffer, toRead);
			}
			pspSdkEnableInterrupts(intr);
			memBuffer = (unsigned int*) ((unsigned int) memBuffer + toRead);
		}
	}

	sceKernelDcacheWritebackInvalidateRange((void*) patchinfo->memOffset, patchinfo->size);
	sceKernelIcacheInvalidateRange((void*) patchinfo->memOffset, patchinfo->size);
}

void applyPatch(SceUID fd, SceModule2* mod) {
	struct PatchHeader head;
	if (sceIoRead(fd, &head, sizeof(head)) < sizeof(head)) {
		DPRINT("Patch missing header.\n");
		return;
	}

	if (head.magic != MAGIC) {
		DPRINT("Patch has malformed magic number.\n");
		return;
	}

	int patchNumber;
	for (patchNumber = 0; patchNumber < head.nPatches; ++patchNumber) {
		int offset = sizeof(struct PatchHeader) + sizeof(struct MemPatch) * patchNumber;
		if (sceIoLseek32(fd, offset, PSP_SEEK_SET) != offset) {
			DPRINT("Unknown error.\n");
			return;
		}

		struct MemPatch patchinfo;
		if (sceIoRead(fd, &patchinfo, sizeof(patchinfo)) != sizeof(patchinfo)) {
			DPRINT("Premature end of file.\n");
			return;
		}

		if (patchinfo.flags & TEXT_REL) {
			patchinfo.memOffset += mod->text_addr;
		}

		patchMemory(fd, &patchinfo);
	}
}
