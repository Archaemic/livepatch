LivePatch
=========
An on-the-fly patching utility for the PlayStation Portable

Version 0.1.3

-------------------------------------------------------------------------------

About
-----

LivePatch is a generic utility for enabling mod developers and hackers to apply
patches to legally owned UMDs or PSN titles when the game runs, instead of
having to patch and use ISOs or CSOs. This allows for easier game patching, as
patch files are generally smaller than full ISOs, as well as not requiring the
distribution of ISOs or clunky patching utilities. It's just easier for the
user!

LivePatch is an open source project, and the source can be found online on
[BitBucket]( http://bitbucket.org/Archaemic/livepatch )

Installing LivePatch
--------------------

LivePatch requires a PSP running custom firmware. If you don't know if you are
running custom firmware, you probably are not, and must do so. Installing
custom firmware is beyond the scope of this README, so please find instructions
to do so elsewhere first.

Once you are running a custom firmware, create a folder called livepatch at the
root of your memory stick and put the file livepatch.prx inside this folder.

Next, you must ensure there is a folder in the root of the memory stick called
seplugins. If it does not exist, create it.

Open seplugins/GAME.TXT (you must create the file if it doesn't exist already)
and add a line:

	ms0:/livepatch/livepatch.prx 1

You are now ready to install patches.

Installing patches
------------------

Patches are placed within folders within the livepatch folder. Each patch
folder corresponds to a game. The name of the patch folder to make within the
livepatch folder should be given to you by the patch developers. Note that the
folder name must be consistent with what the patch developer tells you, or
LivePatch will not be able to find the patches for your game. Every patch
within the corresponding patch folder will be loaded by the game when it is
started, so do not place patches in the patch folder unless you want it to be
loaded.

For instance, if you have patches for a game called FooBar, the patch folder
might be called foo. In this case, you would have a folder within the livepatch
folder at the root of your memory stick called foo:

	ms0:/livepatch/foo

Each file within the patch folder will be loaded when the game starts, so if
you have patches a.live, b.live and c.live, they would be placed like this:

	ms0:/livepatch/foo/a.live
	ms0:/livepatch/foo/b.live
	ms0:/livepatch/foo/c.live

If you didn't want one or more of these patches to be loaded, you should delete
the patch file in the patch folder.

Patch file format
-----------------

This section is only relevant to patch creators. If you aren't creating a patch,
this information isn't relevant to you.

Currently, patches can only apply to the game's main executable, as LivePatch
works by patching memory directly when the game starts. A later version will
support patching files as they are loaded.

For patch developers, the patch format is documented below. All values are
little endian (LSB).

The header is sixteen bytes. In order:

* 0: The "magic number" and should always be "Live" (4C 69 76 65)

* 4: The patch format version. This is currently version 0, so leave those four
  bytes as 0.

* 8: The number of memory address blocks to be patched.

* 12: Reserved for future revisions and should be left as all 0s.

The following block of the file contains information about each memory section
to be patched. For each address block, there is a sixteen byte structure:

* 0: The offset within the file that the patch starts.

* 4: The offset within memory that the patch starts.

* 8: The size of memory to be patched.

* 12: Flags about the patch, ORed together.

There are currently 3 different flags that can be set:

* Bit 0: The patch should be applied as a sequence of XORs instead of
  overwriting the values directly. This minimizes the amount of information
  that needs to be duplicated from the original memory.

* Bit 1: The patch is NULL data. There is no corresponding offset in the file,
  and the memory block should be zeroed out.

* Bit 2: The memory address is relative to the start of the .text segment of
  the module when loaded into memory, instead of an absolute address.

LivePatch determines which folder to load the patches from by using the names
of modules loaded at runtime. If your game is called FooBar, and it loads a
module named foo at runtime, the folder you will put patches in will be:

	ms0:/livepatch/foo

If you have a debug version of LivePatch, it will put a log on the memory stick
at ms0:/livepatch/log.txt

Copyright
---------

LivePatch is copyright © 2012 Archaemic, and released under the license as
specified in the included LICENSE file.
